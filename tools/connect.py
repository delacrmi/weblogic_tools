#!/usr/bin/env python2.7
# user, password, t3://host:port, role

from java.io import FileInputStream
import java.lang
import os
import string

propInputStream = FileInputStream("./tools/weblogic.properties")
configProps = Properties()
configProps.load(propInputStream)

USERNAME = configProps.get("USERNAME")
PASSWORD = configProps.get("PASSWORD")
HOST = configProps.get("HOST")
PORT = configProps.get("PORT")

try:
    connect(USERNAME, PASSWORD, 't3://%s:%s' %
            (HOST, PORT), adminServerName='AdminServer')
except Exception:
    print 'Connection error'
    exit()
