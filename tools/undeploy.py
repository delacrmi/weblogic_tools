#!/usr/bin/env python2.7
from java.io import FileInputStream
import java.lang
import os
import string

propInputStream = FileInputStream("./tools/weblogic.properties")
configProps = Properties()
configProps.load(propInputStream)

APP_NAME = configProps.get("APP_NAME")

try:
    undeploy(APP_NAME)
except Exception:
    print 'error undeploy'
    exit()
