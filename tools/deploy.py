#!/usr/bin/env python2.7
from java.io import FileInputStream
import java.lang
import os
import string

propInputStream = FileInputStream("./tools/weblogic.properties")
configProps = Properties()
configProps.load(propInputStream)

APP_NAME = configProps.get("APP_NAME")
TARGET_PATH = configProps.get("TARGET_PATH")
TARGET = configProps.get("TARGET")

try:
    deploy(APP_NAME, TARGET_PATH, targets=TARGET)
except Exception:
    print 'error deploy'
    exit()
