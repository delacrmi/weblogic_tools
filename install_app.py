#!/usr/bin/env python2.7

execfile('./tools/connect.py')

execfile('./tools/deploy.py')

execfile('./tools/start_application.py')

execfile('./tools/disconnect.py')
