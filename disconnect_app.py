#!/usr/bin/env python2.7

execfile('./tools/connect.py')

execfile('./tools/stop_application.py')

execfile('./tools/undeploy.py')

execfile('./tools/disconnect.py')
